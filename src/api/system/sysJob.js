import request from '@/utils/request'

export function findJobByCpyCode() {
  return 'admin-service/api/sysJobMng/findJobByCpyCode'
}
export function loadDataUrl() {
  return 'admin-service/api/sysJobMng/loadData'
}

export function deleteUrl() {
  return 'admin-service/api/sysJobMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/sysJobMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/sysJobMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/sysJobMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/sysJobMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/sysJobMng/doDelete',
    method: 'post',
    data
  })
}
